<?php

namespace CreateApiInOffice\Controllers;

use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
abstract class AbstractController
    implements InterfaceController
{
    /**
     *
     */
    public function __construct() {}

    /**
     * @param InterfaceRequest $request
     * @param string           $Path
     *
     * @return string
     */
    final public function execute(InterfaceRequest $request, string $Path): string
    {
        $methods = get_class_methods($this);
        unset($methods[0]);
        unset($methods[1]);

        foreach ($methods as $key => $value) {
            if ($Path === $value) {
                if (method_exists($this, $value)) {
                    return $this->$value($request);
                }
            }
        }

        return "";
    }
}
