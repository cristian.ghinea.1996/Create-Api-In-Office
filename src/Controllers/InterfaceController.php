<?php

namespace CreateApiInOffice\Controllers;

use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
interface InterfaceController
{
    /**
     * @param InterfaceRequest $request
     * @param string           $Path
     *
     * @return string
     */
    public function execute(InterfaceRequest $request, string $Path): string;
}
