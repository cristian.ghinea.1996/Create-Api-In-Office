<?php

namespace CreateApiInOffice\Router\Routes;

use CreateApiInOffice\Router\Routes\Route\InterfaceRoute;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
final class Routes
    implements InterfaceRoutes
{
    /**
     * @param InterfaceRequest $request
     *
     * @return void
     */
    public function execute(InterfaceRequest $request): void
    {
        // TODO: Implement run() method.
    }

    /**
     * @param InterfaceRoute $route
     *
     * @return void
     */
    public function addRoute(InterfaceRoute $route): void
    {
        // TODO: Implement addRoute() method.
    }
}
