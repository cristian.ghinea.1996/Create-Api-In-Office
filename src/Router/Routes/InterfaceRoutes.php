<?php

namespace CreateApiInOffice\Router\Routes;

use CreateApiInOffice\Router\Routes\Route\InterfaceRoute;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
interface InterfaceRoutes
{
    /**
     * @param InterfaceRequest $request
     *
     * @return void
     */
    public function execute(InterfaceRequest $request): void;

    /**
     * @param InterfaceRoute $route
     *
     * @return void
     */
    public function addRoute(InterfaceRoute $route): void;
}

