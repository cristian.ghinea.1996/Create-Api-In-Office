<?php

namespace CreateApiInOffice\Router\Routes\Route;

use CreateApiInOffice\Controllers\AbstractController;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
final class Route
    implements InterfaceRoute
{
    /**
     * @param InterfaceRequest $request
     *
     * @return void
     */
    public function execute(InterfaceRequest $request): void
    {
        // TODO: Implement run() method.
    }

    /**
     * @param AbstractController $controller
     *
     * @return void
     */
    public function setController(AbstractController $controller): void
    {
        // TODO: Implement setController() method.
    }

    /**
     * @param string $Path
     *
     * @return void
     */
    public function setPath(string $Path): void
    {
        // TODO: Implement setPath() method.
    }
}
