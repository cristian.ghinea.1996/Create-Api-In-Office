<?php

namespace CreateApiInOffice\Router\Routes\Route;

use CreateApiInOffice\Controllers\AbstractController;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
interface InterfaceRoute
{
    /**
     * @param InterfaceRequest $request
     *
     * @return void
     */
    public function execute(InterfaceRequest $request): void;

    /**
     * @param AbstractController $controller
     *
     * @return void
     */
    public function setController(AbstractController $controller): void;

    /**
     * @param string $Path
     *
     * @return void
     */
    public function setPath(string $Path): void;
}
