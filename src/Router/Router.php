<?php

namespace CreateApiInOffice\Router;

use CreateApiInOffice\Router\Routes\InterfaceRoutes;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
final class Router
    implements InterfaceRouter
{
    /**
     * @param InterfaceRoutes $routes
     */
    public function __construct(InterfaceRoutes $routes)
    {
        $this->setRoutes($routes);
    }

    /**
     * @return void
     */
    public function execute(InterfaceRequest $request): void
    {
        // TODO: Implement run() method.
    }

    /**
     * @param InterfaceRoutes $routes
     *
     * @return void
     */
    public function setRoutes(InterfaceRoutes $routes): void
    {
        // TODO: Implement setRoutes() method.
    }
}
