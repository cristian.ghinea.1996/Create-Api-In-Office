<?php

namespace CreateApiInOffice\Router;

use CreateApiInOffice\Router\Routes\InterfaceRoutes;
use CreateApiInOffice\Routes\Route\Request\InterfaceRequest;

/**
 *
 */
interface InterfaceRouter
{
    /**
     * @param InterfaceRequest $request
     *
     * @return void
     */
    public function execute(InterfaceRequest $request): void;

    /**
     * @param InterfaceRoutes $routes
     *
     * @return void
     */
    public function setRoutes(InterfaceRoutes $routes): void;
}
